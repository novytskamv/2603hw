package com.company;

public class Main {

  public static void main(String[] args) {

//    First task

    {
      int[] array = {2, 3, 7, 7, -3, 6, 6, -4, 11, 11, -2, 6};

      boolean ready = false;
      int tmpMaxPlateau = -100000000;
      int tmpEndIndex = 0;
      int tmpBeginIndex = 0;

      for (int i = 2; i < array.length; i++) {
        if (array[i] == array[i - 1] && array[i - 1] > array[i - 2]) {
          ready = true;
        } else if (array[i] == array[i - 1] && array[i - 1] == array[i - 2]) {
        } else if (array[i] < array[i - 1] && array[i - 1] == array[i - 2]) {
          if (ready == true) {
            ready = false;
            if (tmpMaxPlateau < array[i - 1]) {
              tmpMaxPlateau = array[i - 1];
              tmpEndIndex = i - 1;
            }
          }
        } else {
          ready = false;
        }
      }
      for (int i = tmpEndIndex; i > 0; i--) {
        if (array[i] == array[i - 1]) {
          tmpBeginIndex = i - 1;
        } else {
          break;
        }
      }
      System.out.println(
          "Maximum Plateau is " + tmpMaxPlateau + " Indexes from " + tmpBeginIndex + " to "
              + tmpEndIndex);
    }

    //Second task

    {

      int n = 10;

      int probability = 1000;
      int allHeared = 0;
      int sumOfheard = 0;

      for (int i = 0; i < probability; i++) {
        boolean[] isKnow = new boolean[n];
        int rounds = 1;
        int j = n - 2;
        isKnow[n - 1] = true;
        while (isKnow[j] == false) {
          isKnow[j] = true;
          ++rounds;
          j = (int) (Math.random() * (n - 2));
        }
        sumOfheard += rounds;
        if (rounds == n) {
          ++allHeared;
        }
      }
      if (!(allHeared == 0)) {
        System.out.printf("The probability that all guests will hear the rumor is %.2f%%\n",
            ((double) allHeared / probability) * 100);
      } else {
        System.out.println("The probability that all guests will hear the rumor is 0");
      }
      System.out.printf("The expected average number of people to hear the rumor %.2f%%\n",
           (double) sumOfheard / (double) probability / 10 * 100);
    }

//    Third task

    {

      int m = 0;
      int n = 0;
      double p = 0;

      if (args.length == 3) {
        try {
          m = Integer.parseInt(args[0]);
          n = Integer.parseInt(args[1]);
          p = Double.parseDouble(args[2]);

          if (p > 0 && p < 1) {

            boolean[][] minesweeper = new boolean[m + 2][n + 2];
            for (int i = 1; i <= m; i++) {
              System.out.println();
              for (int j = 1; j <= n; j++) {
                double ownP = Math.random();
                if (ownP < p) {
                  minesweeper[i][j] = true;
                } else {
                  minesweeper[i][j] = false;
                }
              }
            }
            System.out.println();
            int minesAround = 0;

            for (int i = 1; i <= m; i++) {
              System.out.println();
              for (int j = 1; j <= n; j++) {
                if (minesweeper[i][j] == false) {
                  if (minesweeper[i - 1][j - 1] == true) {
                    ++minesAround;
                  }
                  if (minesweeper[i - 1][j] == true) {
                    ++minesAround;
                  }
                  if (minesweeper[i - 1][j + 1] == true) {
                    ++minesAround;
                  }
                  if (minesweeper[i][j - 1] == true) {
                    ++minesAround;
                  }
                  if (minesweeper[i][j + 1] == true) {
                    ++minesAround;
                  }
                  if (minesweeper[i + 1][j - 1] == true) {
                    ++minesAround;
                  }
                  if (minesweeper[i + 1][j] == true) {
                    ++minesAround;
                  }
                  if (minesweeper[i + 1][j + 1] == true) {
                    ++minesAround;
                  }
                  System.out.print(minesAround);
                  minesAround = 0;
                } else if (minesweeper[i][j] == true) {
                  System.out.print("*");
                }
              }
            }
          } else {
            System.out.println("Probability should be from 0 to 1");
          }
        } catch (NegativeArraySizeException | NumberFormatException e) {
          System.out.println("Please try again - format of number is not correct");
        }
      } else {
        System.out.println("Please try again - enter 3 arguments");
      }

    }

  }
}
